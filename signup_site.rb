# frozen_string_literal: true

require 'mysql2'
require 'pony'
require 'sinatra/base'
require 'yaml'

# Main class for signup site
class SignupSite < Sinatra::Base
  set :session_store, Rack::Session::Pool
  set :sessions, expire_after: 60 * 60 * 4,
                 same_site: :strict,
                 secure: ENV['RACK_ENV'] == 'production'
  set :title, 'Shell signup - Insomnia 24/7'
  set :gitid, File.read('.current')
  set :config, YAML.load_file('config.yaml')
  set :static, true
  set :static_cache_control, [:public, { max_age: 3600 }]

  get '/' do
    landing   = YAML.load_file('content/landing.yaml')
    @header   = landing['header']
    @blurb    = landing['blurb']
    @body     = landing['body']
    @form     = erb :landing
    @progress = 0
    erb :index
  end
end

require_relative 'helpers/email'
require_relative 'helpers/mysql'
require_relative 'routes/code'
require_relative 'routes/details'
require_relative 'routes/error'
require_relative 'routes/finished'
