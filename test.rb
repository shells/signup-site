# frozen_string_literal: true

require File.expand_path 'test_helper.rb', __dir__

include Rack::Test::Methods

def app
  SignupSite
end

describe 'Landing page' do
  it 'should return HTML' do
    get '/'
    _(last_response.body).must_include '<!DOCTYPE html>'
  end

  it 'should return the landing page' do
    get '/'
    _(last_response.body).must_include 'Welcome to the Insomnia 24/7 shells signup'
  end

  it 'should display info' do
    get '/'
    _(last_response.body).must_include 'About our shells'
  end

  it 'should display link to enter-code' do
    get '/'
    _(last_response.body).must_include 'href="enter-code"'
  end
end

describe 'Enter code page' do
  it 'should return invite code form' do
    get '/enter-code'
    _(last_response.body).must_include 'Invite code'
  end

  it 'should return invite code form' do
    get '/enter-code'
    _(last_response.body).must_include 'Invite code'
  end
end

describe 'Check code page' do
  it 'should reject empty invite codes' do
    post '/check-code'
    follow_redirect!
    _(last_request.url).must_include 'error'
  end

  it 'should reject improperly formatted invite codes' do
    post '/check-code',
         input_invite: 'not an invite code'
    follow_redirect!
    _(last_request.url).must_include 'error'
  end
end

describe 'Enter details page' do
  it 'should display reasons info' do
    get '/enter-details'
    _(last_response.body).must_include 'Specifying your reasons'
  end

  it 'should display details form' do
    get '/enter-details'
    _(last_response.body).must_include 'User name'
  end
end

describe 'Check username' do
  it 'should reject empty usernames' do
    post '/check-details',
         input_email: 'username@example.com'
    follow_redirect!
    _(last_request.url).must_include 'error'
  end

  it 'should reject usernames with invalid characters' do
    post '/check-details',
         input_username: 'user%name',
         input_email: 'username@example.com'
    follow_redirect!
    _(last_request.url).must_include 'error'
  end
end

describe 'Check reason' do
  it 'should warn for empty reasons' do
    post '/check-details',
         input_username: 'username',
         input_email: 'username@example.com'
    follow_redirect!
    _(last_request.url).must_include 'warning'
  end
end

describe 'Check email address' do
  it 'should reject invalid email addresses' do
    post '/check-details',
         input_username: 'username',
         input_email: 'not really an email.address',
         input_reason: 'My reasons are my own.'
    follow_redirect!
    _(last_request.url).must_include 'error'
  end
end

describe 'Error and warning pages' do
  it 'should display error message from session' do
    get '/error', {}, 'rack.session' => { error_message: 'This is an error' }
    _(last_response.body).must_include 'This is an error'
  end

  it 'should display warning message from session' do
    get '/warning', {}, 'rack.session' => { error_message: 'This is a warning' }
    _(last_response.body).must_include 'This is a warning'
  end
end
