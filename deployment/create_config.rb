# frozen_string_literal: true

require 'yaml'

config = {
  'mysql' => {
    'server'   => '127.0.0.1',
    'port'     => 3306,
    'database' => 'signup_invites',
    'username' => 'signup_invites',
    'password' => ENV.fetch('DB_PASS'),
    'timeout'  => 10
  }
}

puts config.to_yaml
