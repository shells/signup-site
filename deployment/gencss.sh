#!/bin/bash
purgecss --css css/*.css --content views/*.erb --out css/purged/
cat css/purged/*.css > css/combined/combined.css
cleancss -o public/css/combined.min.css css/combined/combined.css
