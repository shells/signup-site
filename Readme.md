# Shell signup site
This website is made to allow users to sign up to the Insomnia 24/7 shell service.

## Build status
Production:  
[![build status](https://git.insomnia247.nl/shells/signup-site/badges/master/build.svg)](https://git.insomnia247.nl/shells/signup-site/commits/master) [![coverage report](https://git.insomnia247.nl/shells/signup-site/badges/master/coverage.svg)](https://git.insomnia247.nl/shells/signup-site/commits/master)

## Requirements

### Ruby

We're developing using Ruby 2.7.4 but it should work with most any modern Ruby version. Get it either from your package manager, brew, rbenv, or any other means you perfer.

### Bundler gem
This is used to install any dependencies required. `gem install bundler` should be all you need to do once your ruby environment is set up.

### Sinatra
We use the Sinatra web framework for pretty much all of this site. Sinatra and all dependencies should be installed by running `bundle install`.

## Running a local development copy

This should be as simple as running `rackup` and going to `http://localhost:9292`.

## Contributing

If you have any changes you would like to make or any bugfixes you want to submit you can fork this project, make your changes, and create a merge request through the gitlab interface. Before creating a merge request make sure all syntax, linting, and tests pass. Do not try to commit to your own master branch as gitlab CI will try and fail to deploy it, causing your builds to fail. Always commit to your own development/feature/fix branch and create your merge request from there.

## Deployment
For deployment you won't need testing and development gems. Run `bundle install --without test development` to install only the runtime required gems.
