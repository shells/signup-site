# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
require 'simplecov'
require 'minitest/autorun'
require 'rack/test'

SimpleCov.start

require File.expand_path 'signup_site.rb', __dir__
