# frozen_string_literal: true

# Invite code handling
class SignupSite < Sinatra::Base
  get '/enter-code' do
    ec        = YAML.load_file('content/enter-code.yaml')
    @header   = ec['header']
    @body     = ec['body']
    @form     = erb :enter_code
    @progress = 25
    erb :index
  end

  post '/check-code' do
    session[:invite] = Rack::Utils.escape_html(params[:input_invite])

    if check_code session[:invite]
      redirect '/enter-details', 302
    else
      cc                       = YAML.load_file('content/check-code.yaml')
      session[:error_message]  = cc['error_message']
      session[:error_location] = '/enter-code'
      session[:error_progress] = 25
      redirect '/error', 302
    end
  end

  private

  def check_code(code)
    return false if code.nil?
    return false if code.match(/^[0-9a-fA-F]{64}$/).nil?

    # Check if code is in database
    mysql  = mysql_connect
    check  = mysql.prepare 'SELECT * FROM invites where `key` = ? LIMIT 1'
    result = check.execute(code).first
    mysql&.close

    return false if result.nil? # No such code in database
    return false unless result['privs'].to_i.zero? # Code has already been used

    session[:email] = Rack::Utils.escape_html(result['email'])
    true
  end
end
