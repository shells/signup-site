# frozen_string_literal: true

# Singup details handling
class SignupSite < Sinatra::Base
  get '/enter-details' do
    ed        = YAML.load_file('content/enter-details.yaml')
    @header   = ed['header']
    @blurb    = ed['blurb']
    @body     = ed['body']
    @form     = erb :enter_details
    @progress = 50
    erb :index
  end

  post '/check-details' do
    cd                       = YAML.load_file('content/check-details.yaml')
    session[:error_location] = '/enter-details'
    session[:error_proceed]  = '/finished'
    session[:error_progress] = 50

    session[:username] = Rack::Utils.escape_html(params[:input_username])
    session[:email]    = Rack::Utils.escape_html(params[:input_email])
    session[:reason]   = Rack::Utils.escape_html(params[:input_reason])

    if session[:username] !~ /^\w+$/
      session[:error_message] = cd['error_message_username']
      redirect '/error', 302
    elsif session[:email] !~ /^\S+@\S+\.\w+$/
      session[:error_message] = cd['error_message_email']
      redirect '/error', 302
    elsif session[:reason].empty?
      session[:error_message] = cd['error_message_reason']
      redirect '/warning', 302
    elsif session[:username] =~ /[A-Z]/
      session[:error_message] = cd['error_message_username_uppercase']
      redirect '/warning', 302
    else
      mysql  = mysql_connect
      check  = mysql.prepare 'SELECT * FROM invites where `username` = ? LIMIT 1'
      result = check.execute(session[:username]).first
      mysql&.close

      unless result.nil?
        session[:error_message] = cd['error_message_username_in_use']
        redirect '/error', 302
      end

      mysql = mysql_connect
      if session.key?(:invite)
        query            = mysql.prepare 'SELECT * FROM invites where `key` = ? LIMIT 1'
        record           = query.execute(session[:invite]).first
        session[:parent] = Rack::Utils.escape_html(record['parent'])
      else
        insert = mysql.prepare 'INSERT INTO invites (`email`, `key`, `parent`) VALUES (?, ?, ?)'
        insert.execute session[:email], 'No invite code', 'SignupSite'
      end

      update_username = mysql.prepare 'UPDATE `invites` SET `username` = ? WHERE `key` = ?'
      update_privs    = mysql.prepare 'UPDATE `invites` SET `privs` = ? WHERE `key` = ?'
      update_username.execute(session[:username], session[:invite])
      update_privs.execute('2', session[:invite])

      mysql&.close

      email_send

      redirect '/finished', 302
    end
  end
end
