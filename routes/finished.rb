# frozen_string_literal: true

# Finished page
class SignupSite < Sinatra::Base
  get '/finished' do
    fi        = YAML.load_file('content/finished.yaml')
    @header   = fi['header']
    @body     = fi['body']
    @form     = erb :finished
    @progress = 100
    erb :index
  end
end
