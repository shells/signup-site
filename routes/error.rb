# frozen_string_literal: true

# Invite code handling
class SignupSite < Sinatra::Base
  get '/error' do
    @header                     = 'Oh no!'
    @body                       = session[:error_message]
    @form                       = erb :error
    @progress                   = session[:error_progress]
    session[:error_reload_form] = true
    erb :index
  end

  get '/warning' do
    @header                     = 'Look out!'
    @body                       = session[:error_message]
    @form                       = erb :warning
    @progress                   = session[:error_progress]
    session[:error_reload_form] = true
    erb :index
  end
end
