# frozen_string_literal: true

# MySQL helper class
class SignupSite < Sinatra::Base
  helpers do
    def mysql_connect
      Mysql2::Client.new(
        host: settings.config['mysql']['server'],
        port: settings.config['mysql']['port'],
        database: settings.config['mysql']['database'],
        username: settings.config['mysql']['username'],
        password: settings.config['mysql']['password'],
        connect_timeout: settings.config['mysql']['timeout']
      )
    rescue Mysql2::Error
      halt erb 'Database error'
    end
  end
end
