# frozen_string_literal: true

# Mail helper class
class SignupSite < Sinatra::Base
  helpers do
    def email_send
      Pony.mail(
        to: 'coolfire@insomnia247.nl',
        from: 'no-reply@insomnia247.nl',
        reply_to: session[:email],
        subject: "Signup request for: #{session[:username]}",
        body: (erb :email),
        via: :smtp,
        via_options: {
          address: 'mail.insomnia247.nl',
          enable_starttls_auto: true
        }
      )
    rescue Net::SMTPFatalError
      halt erb 'Email error'
    end
  end
end
